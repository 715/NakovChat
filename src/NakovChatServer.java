/**
 * Nakov Chat Server
 * <p>
 * (c) Svetlin Nakov, 2002
 * <p>
 * http://www.nakov.com
 * <p>
 * <p>
 * <p>
 * Nakov Chat Server is multithreaded chat server. It accepts multiple clients
 * <p>
 * simultaneously and serves them. Clients can send messages to the server.
 * <p>
 * When some client send a message to the server, this message is dispatched
 * <p>
 * to all the clients connected to the server.
 * <p>
 * <p>
 * <p>
 * The server consists of two components - "server core" and "client handlers".
 * <p>
 * <p>
 * <p>
 * The "server core" consists of two threads:
 * <p>
 * - NakovChatServer - accepts client connections, creates client threads to
 * <p>
 * handle them and starts these threads
 * <p>
 * - ServerDispatcher - waits for a messages and sends arrived messages to
 * <p>
 * all the clients connected to the server
 * <p>
 * <p>
 * <p>
 * The "client handlers" consist of two threads:
 * <p>
 * - ClientListener - listens for message arrivals from the socket and
 * <p>
 * forwards them to the ServerDispatcher thread
 * <p>
 * - ClientSender - sends messages to the client
 * <p>
 * <p>
 * <p>
 * For each accepted client, a ClientListener and ClientSender threads are
 * <p>
 * created and started. A ClientInfo object is also created to contain the
 * <p>
 * information about the client. Also the ClientInfo object is added to the
 * <p>
 * ServerDispatcher's clients list. When some client is disconnected, is it
 * <p>
 * removed from the clients list and both its ClientListener and ClientSender
 * <p>
 * threads are interrupted.
 * <p>
 * <p>
 * <p>
 * <p>
 * <p>
 * NakovChatServer class is entry point for the program. It opens a server
 * <p>
 * socket, starts the dispatcher thread and infinitely accepts client connections,
 * <p>
 * creates threads for handling them and starts these threads.
 */

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class NakovChatServer {
    private static final int LISTENING_PORT = 2002;

    public static void main(String[] args) {
        // Open server socket for listening
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(LISTENING_PORT);
            System.out.println("NakovChatServer started on port " + LISTENING_PORT);
        } catch (IOException se) {
            System.err.println("Can not start listening on port " + LISTENING_PORT);
            se.printStackTrace();
            System.exit(-1);
        }
        // Start ServerDispatcher thread
        ServerDispatcher serverDispatcher = new ServerDispatcher();
        serverDispatcher.start();
        // Accept and handle client connections
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                ClientInfo clientInfo = new ClientInfo();
                clientInfo.mSocket = socket;
                ClientListener clientListener =
                        new ClientListener(clientInfo, serverDispatcher);
                ClientSender clientSender =
                        new ClientSender(clientInfo, serverDispatcher);
                clientInfo.mClientListener = clientListener;
                clientInfo.mClientSender = clientSender;
                clientListener.start();
                clientSender.start();
                serverDispatcher.addClient(clientInfo);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}