/**
 * Nakov Chat Server
 * <p>
 * (c) Svetlin Nakov, 2002
 * <p>
 * <p>
 * <p>
 * ServerDispatcher class is purposed to listen for messages received
 * <p>
 * from clients and to dispatch them to all the clients connected to the
 * <p>
 * chat server.
 */

import java.net.Socket;
import java.util.Vector;

public class ServerDispatcher extends Thread {
    private Vector<String> mMessageQueue = new Vector<String>();
    private Vector<ClientInfo> mClients = new Vector<ClientInfo>();
    /**
     * Adds given client to the server's client list.
     */

    synchronized void addClient(ClientInfo aClientInfo) {
        mClients.add(aClientInfo);
    }

    /**
     * Deletes given client from the server's client list
     * <p>
     * if the client is in the list.
     */

    synchronized void deleteClient(ClientInfo aClientInfo) {
        int clientIndex = mClients.indexOf(aClientInfo);
        if (clientIndex != -1) {
            mClients.removeElementAt(clientIndex);
        }
    }

    /**
     * Adds given message to the dispatcher's message queue and notifies this
     * <p>
     * thread to wake up the message queue reader (getNextMessageFromQueue method).
     * <p>
     * dispatchMessage method is called by other threads (ClientListener) when
     * <p>
     * a message is arrived.
     */

    synchronized void dispatchMessage(ClientInfo aClientInfo, String aMessage) {
        Socket socket = aClientInfo.mSocket;
        String senderIP = socket.getInetAddress().getHostAddress();
        String senderPort = "" + socket.getPort();
        aMessage = senderIP + ":" + senderPort + " : " + aMessage;
        mMessageQueue.add(aMessage);
        notify();
    }

    /**
     * @return and deletes the next message from the message queue. If there is no
     * <p>
     * messages in the queue, falls in sleep until notified by dispatchMessage method.
     */

    private synchronized String getNextMessageFromQueue() throws InterruptedException {
        while (mMessageQueue.size() == 0) {
            wait();
        }
        String message = mMessageQueue.get(0);
        mMessageQueue.removeElementAt(0);
        return message;
    }

    /**
     * Sends given message to all clients in the client list. Actually the
     * <p>
     * message is added to the client sender thread's message queue and this
     * <p>
     * client sender thread is notified.
     */

    private synchronized void sendMessageToAllClients(String aMessage) {
        for (ClientInfo clientInfo : mClients) {
            clientInfo.mClientSender.sendMessage(aMessage);
        }
    }

    /**
     * Infinitely reads messages from the queue and dispatch them
     * <p>
     * to all clients connected to the server.
     */

    public void run() {
        try {
            while (true) {
                String message = getNextMessageFromQueue();
                sendMessageToAllClients(message);
            }
        } catch (InterruptedException ie) {
            // Thread interrupted. Stop its execution
        }
    }

}