/**
 * Nakov Chat Client
 * <p>
 * (c) Svetlin Nakov, 2002
 * <p>
 * http://www.nakov.com
 * <p>
 * <p>
 * <p>
 * NakovChatClient connects to Nakov Chat Server and prints all the messages
 * <p>
 * received from the server. It also allows the user to send messages to the
 * <p>
 * server. NakovChatClient thread reads messages and print them to the standard
 * <p>
 * output. Sender thread reads messages from the standard input and sends them
 * <p>
 * to the server.
 */

import java.io.*;
import java.net.Socket;

public class NakovChatClient {
    private static String SERVER_HOSTNAME = "localhost";
    private static int SERVER_PORT = 2002;

    public static void main(String[] args) {
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            // Connect to Nakov Chat Server
            SERVER_HOSTNAME = args.length > 0 ? args[0] : "localhost";
            SERVER_PORT = args.length > 1 ? Integer.parseInt(args[1]) : 2002;
            Socket socket = new Socket(SERVER_HOSTNAME, SERVER_PORT);
            in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(
                    new OutputStreamWriter(socket.getOutputStream()));
            System.out.println("Connected to server " +
                    SERVER_HOSTNAME + ":" + SERVER_PORT);
        } catch (IOException ioe) {
            System.err.println("Can not establish connection to " +
                    SERVER_HOSTNAME + ":" + SERVER_PORT);
            ioe.printStackTrace();
            System.exit(-1);
        }
        // Create and start Sender thread
        NakovChatClientSender sender = new NakovChatClientSender(out);
        sender.setDaemon(true);
        sender.start();
        try {
            // Read messages from the server and print them
            String message;
            while ((message = in.readLine()) != null) {
                System.out.println(message);
            }
        } catch (IOException ioe) {
            System.err.println("Connection to server broken.");
            ioe.printStackTrace();
        }
    }
}
